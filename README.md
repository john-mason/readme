# John Mason's README

Hi there! This personal README is inspired by [Darren's](https://about.gitlab.com/handbook/marketing/readmes/dmurph/), [Matej's](https://gitlab.com/matejlatin/focus/) and [Rayana's](https://gitlab.com/rayana/readme/) README.

My goal is to be as transparent as possible with [how I work](https://about.gitlab.com/company/culture/all-remote/asynchronous/#communicate-your-work-preferences), even if this page is a work in progress. :smile:

* GitLab handle: [@john-mason](https://gitlab.com/john-mason)
* Slack handle: [@jmason](https://app.slack.com/client/T02592416/C3TMLK465/user_profile/U02EY3JR02C)
* Team page: [Global Search](https://about.gitlab.com/handbook/engineering/development/enablement/search/)


## Working hours

- My timezone is [Eastern Time](https://time.is/ET)
- I am smart at the beginning of the day and my intelligence steadily **decreases** throughout the day, so I try to wake up early and get started working on hard things as soon as I can. :coffee: :zap:
- If I can, I try to start my day around 7:00am.

## Weekly schedule

- **Monday: Admin day**.  :point_left: Best day for meetings :calendar:
    - Prep work so I can have a productive week.
    - I like to have "Monday Meeting Manias" and consolidate as many meetings as possible to this day.
        - This is my preferred day to have synchronous meetings.
        - This is the day I watch any recorded videos of meetings I didn't get a chance to attend
        - This is the day I will most likely contribute to [async meeting agendas](https://about.gitlab.com/company/culture/all-remote/asynchronous/#examples-of-asynchronous-integration-on-gitlab-teams).
    - If I need to run any personal errands that need to be done during working hours, I try to schedule them on Mondays.
- **Tuesday-Wednesday: Focus days** :bulb:
    - By moving as many meetings as possible to Monday, that leaves these days to do [deep work](https://blog.doist.com/deep-work/).
    - I use [Clockwise](https://chrome.google.com/webstore/detail/clockwise-time-management/hjcneejoopafkkibfbcaeoldpjjiamog?hl=en-US) to automatically block off focus time
    - Clockwise also automatically updates my Slack status to have a :bulb: whenever I am scheduled to focus on deep work.
- **Thursday: Admin day**.
    - Ideally, most meetings and adhoc work are consolidated to Monday. I keep this day open for admin work so I can really focus on Fridays.
    - If you would like to schedule time with me and can't find a time on Monday, try Thursday. :wink:
- **Friday: Focus day** :bulb:
    - [Focus Friday](https://about.gitlab.com/handbook/communication/#focus-fridays) where I ideally have zero meetings and can do some serious deep work. :muscle:


## Working style
- I am an introvert at heart and prefer one on one conversations over big crowds.
    - I really appreciate public praise, but I most cherish direct compliments.
- I learn best by doing.
- I thrive on **momentum** and like to have a [bias towards action](https://about.gitlab.com/handbook/values/#bias-for-action).
- I do my best to offer solutions instead of problems
  - Note on this: I have received feedback before that when I ask for feedback I have already made up my mind.
    - If I'm asking for feedback, I have most likely spent some time thinking about the best way forward.
    - I probablly think the thing I'm suggesting is a good idea, but I recognize I may be missing something so I'm asking for your opinion.
    - If you think I'm not giving enough space for you to give your opinion, *please, please* say you need time to think things over or send me a DM.
- I love to have attention to detail (read: perfectionist), and I actively have to remind myself **progress over perfection**.


## Feedback
- Please feel free to be candid with me. I am an analytical person that is always looking to optimize how I work.
